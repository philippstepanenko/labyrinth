const res = {
	status: true,
	res: {
		game: {
			id: 123,
			round: 0
		},
		map: [[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
			  [1, 0, 0, 0, 0, 0, 1, 2, 0, 1],
			  [1, 1, 1, 0, 1, 0, 1, 1, 0, 1],
			  [1, 0, 1, 0, 1, 0, 1, 0, 0, 1],
			  [1, 0, 1, 0, 0, 0, 1, 0, 1, 1],
			  [1, 0, 1, 0, 1, 1, 1, 0, 1, 1],
			  [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
			  [1, 0, 1, 0, 1, 1, 1, 1, 0, 1],
			  [1, 0, 1, 0, 4, 1, 2, 0, 0, 1],
			  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
			  ],
		mode_graphics: 1,
		players:[
		{
			id: 0,
			name: "Admin",
			x: 1,
			y: 1,
			steps: 0,
			energy: 1,
			texture: "players/player1.png",
			color: "#0000aa"
		},
		{
			id: 112,
			name: "Moderator",
			x: 1,
			y: 3,
			steps: 0,
			energy: 1,
			texture: "players/player2.png",
			color: "#aa0000"
		},
		{
			id: 114,
			name: "Moderator2",
			x: 5,
			y: 4,
			steps: 0,
			energy: 1,
			texture: "players/player3.png",
			color: "#00aa00"
		}
		]
	}
}
