const tex =	[
{
	empty:     "textures/empty.png",
	wall:      "textures/wall.png",
	exit:      "textures/exit.png",
	used_exit: "textures/used_exit.png",
	trap:      "textures/trap.png",
	used_trap: "textures/used_trap.png",
	smoke:     "textures/smoke.png",
	background:"textures/background.png"
}
]