const response = res;

const TEXTURES = tex;

// клавиши направления движения
const directions = {
	left: 37, up: 38, right: 39, down: 40};

// клавиши действия с другими игроками
const actions = {
	swap: 49, fight: 50};

// Коды для идентификации и визуализации (коды ячеек)
const EMPTY     = 0;
const WALL      = 1;
const EXIT      = 2;
const USED_EXIT = 3;
const TRAP      = 4;
const USED_TRAP = 5;

// прочие коды для визуализации
const SMOKE = 6;
const BACKGROUND = 7;

// цвета
const DATA_COLORS = {
	empty:     "#ffffff",
	wall:      "#cccc66",
	exit:      "#00aa00",
	used_exit: "#aa0000",
	trap:      "#00aa00",
	used_trap: "#000000",
	smoke:     "#666666",
	background:"#000000"
};

// преобразование ассоциативного массива в обычный массив
const COLORS = Object.values(DATA_COLORS);

class Controller{
	constructor(directions, actions){
		this.keys = {...directions, ...actions};
		this.directions = directions;
		this.actions = actions;
		this.active = false;
		this.submode = false;
		//this.out-of-turn
	}

	validKey(k){
		// режим взаимодействия персонажей
 		return this.submode ? Object.values(this.actions).indexOf(k) != -1 :
 							  Object.values(this.directions).indexOf(k) != -1;
	}

	validMove(k){
		//console.log("validMove");
		//console.log(Object.values(this.directions).indexOf(k));
		return !this.submode ? Object.values(this.directions).indexOf(k) != -1 : false;
	}

	validAction(k){
		return this.submode ? Object.values(this.actions).indexOf(k) != -1 : false;
	}

	switchActive(){
		return this.active = !this.active;
	}

	restart(t){
		// временная деактивация контроллера для задержки картинки
		this.switchActive();
		// активация контроллера
		setTimeout(() => this.switchActive(),t);
		return true;
	}
}

class Graphics{
	constructor(mode, colors, textures, k){
		this.mode = mode;
		this.colors = colors;
		this.textures = textures;
		this.k = k;
	}
}

class GameObject{
	constructor(x,y){
		this.x = x;
		this.y = y;
		this.color = null;
		this.texture = null;
	}

	view(code, graphics){
		this.color = graphics.colors[code];

		if (graphics.mode > 0) this.texture = Object.values(graphics.textures[graphics.mode-1])[code];
		else this.texture = graphics.textures[0][code];
	}
}

class Render{
	constructor(canvas, out, graphics){
		this.canvas = canvas;
		this.ctx = canvas.map((it)=> it.getContext('2d'));
		this.out = out;

		this.mode = graphics.mode;
		this.colors = graphics.colors;
		this.textures = graphics.textures;
		this.k = graphics.k;

		this.players = null;

		this.w = null;
		this.h = null;

		this.level = null;
		this.init = false;
	}

	initRender(level, players){
		this.level = level;

		this.players = players;

		this.w = level.width*this.k;
		this.h = level.height*this.k;

		this.canvas.map((it)=> it.width=this.w);
		this.canvas.map((it)=> it.height=this.h);

		this.ctx.map((it)=> it.font = this.div(k,3)+"px monospace");

		this.init = true;
	}

	drawOptimize(lastPlayer, coordinates){
		this.drawArea(lastPlayer);
		this.clearPlayer(lastPlayer,coordinates);
		this.drawPlayer(lastPlayer);

		this.players.forEach((it)=>{
			if(coordinates.x == it.x &&
			   coordinates.y == it.y)
				this.drawPlayer(it);
		});
	}

	drawArea(p){
		const x = p.x;
		const y = p.y;
		const cells = [];

		// сверху
		if (y>0) cells.push([y-1,x]);
		// слева
		if (x>0) cells.push([y,x-1]);
		// под игроком
		cells.push([y,x]);
		// справа
		if (x<this.level.width-1) cells.push([y,x+1]);
		// снизу
		if (y<this.level.height-1) cells.push([y+1,x]);

		// перебор соседних клеток
		cells.forEach((it) => {
			// инициализация объекта для рисования
			const obj = new GameObject(it[1],it[0]);
			// туман
			if (this.level.isSmoke(obj.y,obj.x)) obj.view(SMOKE,graphics);
			// не туман
			else obj.view(this.level.map[obj.y][obj.x],graphics);

			// рисование объекта уровня
			this.drawObject(obj);
		});
	}

	drawLevel(){
		// проверка инициализации render
		if(!this.init) return false;

		// инициализация размеров уровня
		const {w, h, k} = this;

		// рисование объектов уровня
		for(let i=0; i<this.level.height;i++){
			for(let j=0; j<this.level.width;j++){

				// координаты
				const x = j;
				const y = i;

				// инициализация объекта для рисования
				const obj = new GameObject(x,y);

				// туман
				if (this.level.isSmoke(i,j)) obj.view(SMOKE,graphics);
				// не туман
				else obj.view(this.level.map[i][j],graphics);

				// рисование объекта уровня
				this.drawObject(obj);
			}
		}
		return true;
	}

	// рисование объекта
	drawObject(obj){
		// проверка инициализации render
		if(!this.init) return false;

		const ctx = this.ctx[0];

		const k = this.k;
		
		if (this.mode > 0 ) {
			const x = obj.x * k;
			const y = obj.y * k;
			// Создает новое изображение
			let img = new Image();

			img.addEventListener("load", () => {
  				ctx.drawImage(img,parseInt(x),parseInt(y),k,k);
			}, false);

			img.src = obj.texture;
		}else{
			const x = obj.x * k + 1;
			const y = obj.y * k + 1;

			ctx.fillStyle = obj.color;
			ctx.fillRect(x,y,k-2,k-2);
		}

		return true;
	}

	clearCtx(ctx){
		ctx.clearRect(0, 0, this.w,this.h);
	}

	drawPlayers(){
		this.clearCtx(this.ctx[1]);
		this.clearCtx(this.ctx[2]);
		
		this.players.forEach((it) => this.drawPlayer(it));
	}

	// стирание персонажа
	clearPlayer(lastPlayer, coordinates){
		// проверка инициализации render
		if(!this.init) return false;

		const k = this.k;
		const x = coordinates.x * k;
		const y = coordinates.y * k;

		this.clearPlayerBody(x,y,k);

		this.clearPlayerLabel(lastPlayer,x,y,k);

		return true;
	}

	// рисование игрока
	drawPlayer(p){
		// проверка инициализации render
		if(!this.init) return false;

		const k = this.k;
		const x = p.x * k;
		const y = p.y * k;

		if (!p.isEscaped()){
			this.drawPlayerBody(p,x,y,k);

			this.drawPlayerLabel(p,x,y,k);

		}
		return true;
	}

	// стирание имени игрока
	clearPlayerLabel(p,x,y,k){
		// проверка инициализации render
		if(!this.init) return false;

		const ctx = this.ctx[2];

		const rect_width = this.div(k,6)*(p.name.length+2);

		ctx.clearRect(x + this.div(k, 2),
						  y + this.div(k, 2) - this.div(k, 3),
						  rect_width,
						  k - this.div(k, 2));

		return true;
	}

	// рисование имени игрока
	drawPlayerLabel(p,x,y,k){
		// проверка инициализации render
		if(!this.init) return false;

		const ctx = this.ctx[2];

		ctx.globalAlpha=0.7;
		ctx.fillStyle="black";
		const rect_width = this.div(k,6)*(p.name.length+2);

		ctx.fillRect(x + this.div(k, 2),
						  y + this.div(k, 2) - this.div(k, 3),
						  rect_width,
						  k - this.div(k, 2));

		ctx.globalAlpha=1;

		ctx.fillStyle="white";
		ctx.fillText(p.name,
						  x + this.div(k, 2) + this.div(k, 12),
						  y + this.div(k, 2));

		return true;
	}

	// рисование тела игрока
	clearPlayerBody(x,y,k){
		if(!this.init) return false;

		const ctx = this.ctx[1];

		ctx.clearRect(x, y, k, k);

		return true;
	}

	// рисование тела игрока
	drawPlayerBody(p,x,y,k){
		if(!this.init) return false;

		const ctx = this.ctx[1];

		if (this.mode > 0){
			// Создает новое изображение
			let img = new Image();

			img.addEventListener("load", () => {
  				ctx.drawImage(img,parseInt(x),parseInt(y),k,k);
			}, false);

			img.src = p.texture;
		}else{
			ctx.fillStyle=p.color;

			ctx.beginPath();
			ctx.ellipse(x + this.div(k, 2),
							 y + this.div(k, 2),
							 k/2-2,
							 k/2-2,
							 Math.PI / 4, 0, 2 * Math.PI);
			ctx.fill();
		}

		return true;
	}

	// вывод строки в out
	showOut(s,c) {
		// const ctx = this.ctx[3];
		// ctx.font = "50px serif";
		// ctx.fillStyle = c;
		// ctx.fillText(s,this.div(this.w,2), this.div(this.h,2));
		this.out.innerHTML += s + "<br>";
		//console.log(s);
	}
	// очистка содержимого out
	clearOut(){
		//const ctx = this.ctx[3];
		//ctx.clearRect(0,0,ctx.width,ctx.height);
		this.out.innerHTML = "";
	}

	div(x, y){
		return (x-x%y)/y;
	}
}

class SubGame{
	constructor(game, firstPlayer, secondPlayer){ //
		this.players = [firstPlayer, secondPlayer];
		this.numberActivePlayer = 0;
		this.playersActions = [null, null];
		this.gameActions = game.controller.actions;
	}

	// выбор действия игрока
	choosePlayerAction(action){
		// добавление действие персонажу
		this.playersActions[this.numberActivePlayer] = action;
		// смена хода внутри подигры
		this.nextPlayer();
	}
	// подигра может быть завершена
	isFinale(){
		return this.playersActions[0] == this.gameActions.fight ||
			   Object.values(this.gameActions).indexOf(this.playersActions[0])!=-1 &&
			   Object.values(this.gameActions).indexOf(this.playersActions[1])!=-1;
	}

	nextPlayer(){
		this.numberActivePlayer = (this.numberActivePlayer+1) % 2;
	}

	fight(){
		let min = Math.ceil(0);
  		let max = Math.floor(1);
  		let res = Math.floor(Math.random() * 2);
  		this.players[res].energy -= 3;
  		this.players[res].damage = true;

  		this.players[(res+1)%2].energy--;
  		//console.log("Игрок " + this.players[res].id + " проиграл");
	}

	swap(){
		let players = [{x: this.players[0].x,
						y: this.players[0].y},
					   {x: this.players[1].x,
						y: this.players[1].y}]
		let updatePlayers = this.players;
		
		this.players[0].x = players[1].x;
		this.players[0].y = players[1].y;
		this.players[0].energy--;

		this.players[1].x = players[0].x;
		this.players[1].y = players[0].y;
		this.players[1].energy--;
	}

}

class Game{
	constructor(id, round, level){
		this.id = id;
		this.round = round;
		this.level = level;
		this.players = [];
		this.activeGame = false;   // активация игры
		this.numberActivePlayer = null; // номер активного игрока (начиная с 0) по очереди

		this.subGame = null;

		this.render = null;
		this.controller = null;
	}

	//createSubGame(firstPlayer, secondPlayer, action){
	//	this.subGame = new SubGame(firstPlayer, secondPlayer, action);
	createSubGame(game, firstPlayer, secondPlayer){
		this.subGame = new SubGame(game, firstPlayer, secondPlayer);
	}

	activePlayer(){
		return this.players[this.numberActivePlayer];
	}

	// добавление игрока в игру
	addPlayer(player){
		this.players.push(player);
	}

	// проверка наличия противника в соседней клетке
	enemyHere(player, shift){
		// id активного игрока
		const id = player.id;
		// локальная версия активного игрока
		const p = {};
		p.x = player.x;
		p.y = player.y;
		// сдвиг локальной копии активного игрока
		p.x +=shift.x;
		p.y +=shift.y;
		// фильтруем из списка игроков игрока, который делает ход
		const players = this.players.filter((it) => it.id != id);
		// фильтрация списка игроков для проверки на существование в клетке,
		// куда движется игрок, противника без урона
		const res = players.filter((it) => !it.damage && it.x == p.x && it.y == p.y);
		// результат проверки на существование в клетке,
		// куда движется игрок, противника без урона
		return {isExist: res.length > 0,
				enemy: res[0]};
	}

	connectRender(render){
		this.render = render;
		// инициализация render с учётом уровня игры
		this.render.initRender(this.level,this.players);
	}

	connectController(controller){
		this.controller = controller;
		// активация котроллера
		this.controller.active = true;
	}

	// TODO пересмотреть логику обработки нажатий клавиш игроками
	startController(){
		if (this.controller == null) return false;
		// слушатель события
		addEventListener("keyup", (event) => {
			// предотвращение хода во время не активной игры
			if(!this.activeGame) return false;
			// игрок походил?
			let isMove = false;
			// игроку задано действие?
			let isAct = false;
			// проверка активности контроллера
			if (this.controller.active){
				// проверка нажатия клавиши
			    if (this.controller.validKey(event.keyCode) == false){
			    	return false;
			    }
			    // если существует игровая ситуация между игроками
			    if (this.subGame != null){
			    	isAct = true;
		    		// если действие валидно
		    		if (this.controller.validAction(event.keyCode)){
		    			// добавление действия активному игроку игровой ситуации
		    			//console.log("добавление действия активному игроку игровой ситуации");
		    			this.subGame.choosePlayerAction(event.keyCode);
		    		}else{
		    			//console.log("Игрок " + (this.subGame.numberActivePlayer + 1) +" выполнил не валидное действие!");	
		    		}
			    }
			    // координаты для будущего частичного рендеринга
			    const coordinates = this.activePlayer().getCoordinates();
			    // проверка нажатия клавиши направления
			    if (this.controller.validMove(event.keyCode)){
			    	// если в соседней клетке есть противник без урона
			    	const enemy = this.enemyHere(this.activePlayer(),
												 this.activePlayer().shiftPlayer(event.keyCode));
			    	// есть противник?
			    	if (enemy.isExist){
			    		// создание игровой ситуации между игроками
			    		this.createSubGame(this,
			    						   this.activePlayer(),
			    						   enemy.enemy);
			    		//
			    		this.controller.submode = true;
			    		//
			    		isAct = true;
			    	}
			    	else{
			    		// передвинуть персонажа
			    		isMove = this.activePlayer().move(event.keyCode);
			    	}
			    }
			    
			    // проверка выполнения хода игрока
				if(isMove && !isAct){
					// рассеивание клеток вокруг игрока
					this.level.dispersion(this.activePlayer());
					// игрок совершивший ход
					const lastPlayer = this.activePlayer();
					// передача хода следующему игроку
					this.nextPlayer();
					// частичный рендер вокруг игрока
					this.render.drawOptimize(lastPlayer, coordinates);

					// перерисовка персонажей и уровня целиком
					//this.render.drawLevel();
					//this.render.drawPlayers();

					// деактивация и повторная активация контроллера
			    	// для решения проблемы с "клонами" персонажей
			    	this.controller.restart(100);

					// сбежали все кроме последнего?
					if(this.isFinale()) {
						this.activeGame = false;
						this.render.clearOut();
						// определение последнего игрока
						const looser = this.players.findIndex((it)=> !it.isEscaped());
						this.render.showOut("<b>Игра окончена!</b><br>" +
								 "<b style='color:"+this.players[looser].color+"'>"+(looser+1) + " игрок " +
								 "проиграл!</b>"
								 );

						//game.render.showOut("Игра окончена! " + (looser+1) + " игрок проиграл!");
					}
				}

				// проверка выполнения действия игроком во время игрового взаимодействия
				if(isAct){
					//console.log("Было действие:");
					//console.log(event.keyCode);
					//this.render.clearOut();
					if (this.subGame.isFinale()){
						// если совершенно нападение на игрока
			    		if (this.subGame.playersActions[0] == this.controller.actions.fight ||
			    			this.subGame.playersActions[1] == this.controller.actions.fight){
			    			// TODO бой игроков
			    			this.subGame.fight();
			    			//console.log("Бой между игроками завершён");
			    			this.render.showOut("<b>Бой между игроками завершён</b>");
			    		// обмен клетками
						} else {
							// TODO обмен клетками игроков
							this.subGame.swap();
							//console.log("Обмен клетками завершён");
							this.render.showOut("<b>Обмен клетками завершён</b>");
			    		}
						this.subGame = null;
						this.controller.submode = false;
						this.nextPlayer();
					} else{
						//this.render.showOut("<b style='color:"+this.activePlayer().color+"'>Игрок " + (this.numberActivePlayer + 1)+" начал игровое взаимодействие с другим игроком</b>");
						console.log(this.subGame.playersActions);
						console.log(this.controller.actions);
						if(this.subGame.playersActions[0]==null){
							console.log(1);
							this.render.showOut("<b style='color:"+this.activePlayer().color+"'>Игрок " + (this.numberActivePlayer + 1)+" начал игровое взаимодействие с другим игроком</b>");
						}else if(this.subGame.playersActions[0]==this.controller.actions.fight){
							console.log(2);
							this.render.showOut("<b style='color:"+this.activePlayer().color+"'>Игрок нападает на противника");
						}else if(this.subGame.playersActions[0]==this.controller.actions.swap){
							console.log(3);
							this.render.showOut("<b style='color:"+this.activePlayer().color+"'>Игрок предлагает поменяться клетками");
						}
						

					}
					this.render.drawLevel(level);
					this.render.drawPlayers();
				}

				if(!isMove && !isAct){
					this.render.clearOut();
					this.render.showOut("<b style='color:"+this.activePlayer().color+"'>Ход игрока:" + (this.numberActivePlayer + 1)+"<br> Неверный ход! Повторите попытку...</b>");
					this.render.showOut("<b>Энергия: " + this.activePlayer().energy +"</b>");
					//this.render.showOut("Ход игрока: " + (this.numberActivePlayer + 1)+". Неверный ход! Повторите попытку...");
				}
			}

		});
	}


	// новый раунд, после хода каждого игрока
	newRound(){
		this.round++;
		this.players.map((it) => {
			if(!it.isEscaped()) it.energy++;
		});
	}

	isFinale(){
		return this.players.length - this.players.filter((it) => it.isEscaped()).length == 1;
	}

	// переход хода следующему игроку
	nextPlayer(){
		if (!this.activeGame) return false;
		else {
			do{
				this.numberActivePlayer = ((this.numberActivePlayer + 1) % this.players.length);
				if (this.numberActivePlayer == 0) this.newRound();
			}while(!this.activePlayer().isEnergy() || this.activePlayer().isEscaped())

			// проверка на исцеление от травмы
			if (this.activePlayer().energy > 0 &&
				this.activePlayer().damage){
				this.activePlayer().damage = false;
			}

			this.render.clearOut();
			this.render.showOut("<b style='color:"+this.activePlayer().color+"'>Ход игрока:" + (this.numberActivePlayer + 1)+"</b>");
			this.render.showOut("<b>Энергия: " + this.activePlayer().energy +"</b>");
			//this.render.showOut("Ход игрока:" + (this.numberActivePlayer + 1));
			return true;
		}
	}

	// начало игры
	start(){
		//if (this.activePlayer() != null) return false;
		if (this.activePlayer() != null ||
			this.render == null ||
			this.controller == null) {
				// запуск игры невозможен
				return false;
		}
		else this.activeGame = true;
		this.numberActivePlayer = 0;
		this.initGame();

		//console.log("Ход игрока:" + (this.numberActivePlayer + 1));
		this.render.clearOut();
		this.render.showOut("<b style='color:"+this.activePlayer().color+"'>Ход игрока:" + (this.numberActivePlayer + 1)+"</b>");
		this.render.showOut("<b>Энергия: " + this.activePlayer().energy +"</b>");
		//this.render.showOut("Ход игрока:" + (this.numberActivePlayer + 1));

		this.startController();

		return true;
	}

	initGame(){
		this.players.forEach((it) => this.level.dispersion(it));
		this.render.drawLevel(level);
		this.render.drawPlayers();
		// исправление бага с невозможностью походить, если
		// первый игрок с отрицательной или нулевой энергией
		while(!this.activePlayer().isEnergy()){
			this.nextPlayer();
		}
	}
}

class Level{
	constructor(map){
		this.map = map;
		this.smoke = map.map((it) => it.map((jt) => false));
		this.width = map[0].length;
		this.height = map.length;
	}
	// рассеивание тумана с учётом текущих координат игрока
	dispersion(p){
		const x = p.x;
		const y = p.y;
		// сверху
		if (y>0) this.smoke[y-1][x]=true;
		// слева
		if (x>0) this.smoke[y][x-1]=true;
		// под игроком
		this.smoke[y][x]=true;
		// справа
		if (x<this.width-1) this.smoke[y][x+1]=true;
		// снизу
		if (y<this.height-1) this.smoke[y+1][x]=true;
	}

	// проверка на туман
	isSmoke(i, j){
		return (!this.smoke[i][j]) ? true : false;
	}

	// проверка на возможность перемещения игрока в заданные координаты
	coordinateСheck(x,y){
		// не на краю уровня?
		if(x >= 0 && x < this.width &&
		   y >= 0 && y < this.height){
		   	 // если не стена
			if(this.map[y][x]!=WALL) return true;
			else return false;
		}else{
			return false;
		}
	}
}

class Player{
	constructor(id,name,x,y,steps,energy,texture,color,level){
		this.id = id;
		this.name = name;
		this.x = x;
		this.y = y;
		this.steps = steps;
		this.energy = energy;
		this.texture = texture;
		this.color = color;
		this.level = level;
		this.escaped = null; // сбежал или нет
		this.damage = false;
	}

	isEscaped(){
		return this.escaped != null ? true : false;
	}

	// движение игрока
	move(direction){
		// валидность направления
		if(direction in directions){
			console.log("Ошибка направления");
			return false;
		}
		// есть ли энергия на ход
		if(this.isEnergy()){
			// сдвиг
			const shift = this.shiftPlayer(direction);
			// проверка на возможность перемещения игрока
			if (this.level.coordinateСheck(this.x + shift.x, this.y + shift.y)) {
				
				this.step(shift.x, shift.y);

				if(this.level.map[this.y][this.x] == EXIT){
					this.level.map[this.y][this.x] = USED_EXIT;
					this.escaped = 1;
				}else if(this.level.map[this.y][this.x] == TRAP){
					this.level.map[this.y][this.x] = USED_TRAP;
				}

				return true;
			}else{
				return false;
			} 
		}else{
			//game.nextPlayer();
			return false;
		}
	}

	shiftPlayer(direction){
		const shift = {x:0, y:0};

		if(direction == directions.left) shift.x--;
		else if(direction == directions.right) shift.x++;
		else if(direction == directions.up) shift.y--;
		else if(direction == directions.down) shift.y++;

		return shift;
	}

	// засчитывание шага с обновлением характеристик игрока (энергия, количество шагов)
	step(sx, sy){
		this.x += sx;
		this.y += sy;
		this.energy--;
		this.steps++;
	}
	
	// проверка наличия энергии игрока
	isEnergy(){
		return this.energy > 0 ? true : false;
	}
	// вывод информации об игроке
	log(){
		console.log("x: " + this.x);
		console.log("y: " + this.y);
		console.log("e: " + this.energy);
		console.log("s: " + this.steps);
	}
	getCoordinates(){
		return { x: this.x,
				 y: this.y};
	}
}

if (!response.status) console.log("Error!"); 

// создание объекта level
const level = new Level(response.res.map);
// создание объекта game
const game = new Game(response.res.game.id,
					  response.res.game.round,
					  level
	);
// инициализация игроков
for (let i = 0; i < response.res.players.length; i++){
	game.addPlayer(new Player(response.res.players[i].id,
							response.res.players[i].name,
							response.res.players[i].x,
							response.res.players[i].y,
							response.res.players[i].steps,
							response.res.players[i].energy,
							response.res.players[i].texture,
							response.res.players[i].color,
							level
		))	
}

// для рендера 
const k=50;

const canvas = Object.entries(document.getElementsByTagName("canvas")).map((it) => document.getElementById(it[1].id));

const out = document.getElementById("out");

// создание объекта graphics с данными о графике
const graphics = new Graphics(response.res.mode_graphics,
							  COLORS,
							  TEXTURES,
							  k
	);

// создание объекта render
const render = new Render(canvas, out, graphics);
// подключение объекта render к game
game.connectRender(render);

// создание объекта controller
const controller = new Controller(directions, actions);
// подключение объекта controller к game
game.connectController(controller);

// старт игры
game.start();